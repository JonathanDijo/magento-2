<?php
namespace Bepic\Theme\Block;
 
class ContactBlock extends \Magento\Framework\View\Element\Template
{
	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context, 
        \Magento\Framework\Registry $registry
    ) {
    	$this->_registry = $registry;
        parent::__construct($context);
    }

    public function getProductName(){
    	if($this->_registry->registry('productName') !== null){
    		return $this->_registry->registry('productName');
    	} else {
    		return false;
    	}
    }

    public function getProductAttr(){
    	if($this->_registry->registry('productAttributes') !== null){
    		return $this->_registry->registry('productAttributes');
    	} else {
    		return false;
    	}
    }
}