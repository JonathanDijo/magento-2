<?php
 
namespace Bepic\Theme\Controller\Contact;
 
use Magento\Framework\App\Action\Context;
 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
 
    public function __construct(
        Context $context, 
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_registry = $registry;
        parent::__construct($context);
    }
 
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $resultPage = $this->_resultPageFactory->create();

        if(isset($params['productName'])){
            $this->_registry->register('productName', $params['productName']);
        }

        if(isset($params['productAttributes'])){
            $this->_registry->register('productAttributes', $params['productAttributes']);
        }
        
        return $resultPage;
    }
}