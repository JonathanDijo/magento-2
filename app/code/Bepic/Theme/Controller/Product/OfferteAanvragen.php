<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Bepic\Theme\Controller\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Controller\ResultFactory;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class OfferteAanvragen extends \Magento\Checkout\Controller\Cart
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param ProductRepositoryInterface $productRepository
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\RequestInterface $request,
        CustomerCart $cart,
        ProductRepositoryInterface $productRepository,
        ResultFactory $result
    ) {
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart
        );
        $this->productRepository = $productRepository;
        $this->resultRedirect = $result;
        $this->url = $url;
        $this->responseFactory = $responseFactory;
        $this->pageFactory = $pageFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->request = $request;
    }

    /**
     * Initialize product instance from request data
     *
     * @return \Magento\Catalog\Model\Product|false
     */
    protected function _initProduct()
    {
        $productId = (int)$this->getRequest()->getParam('product');
        if ($productId) {
            if($this->productRepository->getById($productId) != null){
                return $this->productRepository->getById($productId);
            }
        }

    }

    /**
     * Add product to shopping cart action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;

            $this->messageManager->addError('Er is iets fout gegaan. Neem contact op met de beheerder.');
        }

        $params = $this->getRequest()->getParams();

        $product = $this->_initProduct();

        //die(var_dump($params));

        /**
         * @todo remove wishlist observer \Magento\Wishlist\Observer\AddToCart
         */
        $this->_eventManager->dispatch(
            'checkout_cart_add_product_complete',
            ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
        );

        $eavModel = $this->_objectManager->create('Magento\Catalog\Model\ResourceModel\Eav\Attribute');
        $eavConfig = $this->_objectManager->create('Magento\Eav\Model\Config');

        /*
            $attributesCodes = array();
            $productAttributeValues = array();
            $attributeValueIds = array();
            $attributes = array();
            $CompleteCollection = array();
        */
        if(isset($params['super_attribute'])){
            foreach($params['super_attribute'] as $attrCode => $value){
                $attributeCodes[] = $eavModel->load($attrCode)->getAttributeCode();
                $attributeValueIds[] = $value;
            }

            foreach($attributeCodes as $code){
                $attr = $eavConfig->getAttribute("catalog_product", $code);
                $attributes[] = $attr->getSource()->getAllOptions();
            }

            foreach($attributeValueIds as $valueId){
                foreach($attributes as $attr){
                    foreach($attr as $a){
                        if($a['value'] == $valueId){
                            $productAttributeValues[] = $a['label'];
                        }
                    }
                }
            }

            foreach($attributeCodes as $code){
                $attributeLabels[] = $product->getResource()->getAttribute($code)->getFrontendLabel();
            }

            for($i = 0;$i<count($attributeLabels);$i++){
                $CompleteCollection[] = array('label' => $attributeLabels[$i], 'value' => $productAttributeValues[$i]);
            }

            if(isset($params['kopen'])){
                $CompleteCollection[] = array('label' => 'Kleur', 'value' => $params['Kleur']);
            } else if(isset($params['huren'])){
                $CompleteCollection[] = array('label' => 'Locatie', 'value' => $params['locatie']);
                $CompleteCollection[] = array('label' => 'Huren vanaf', 'value' => $params['huren_vanaf']);
                $CompleteCollection[] = array('label' => 'Huren tot', 'value' => $params['huren_tot']);
            }

            $productName = $product->getName();
            $productAttributes = $CompleteCollection;
            $parameters = array('productName' => $productName, 'productAttributes' => $productAttributes);
        } else {
            $productName = $product->getName();
            $parameters = array('productName' => $productName);
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('contact/contact/',array('_query' => $parameters, '_current' => true));
        return $resultRedirect;
    }
}
